package com.wxsoftware.developerportfolio.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wxsoftware.developerportfolio.views.AboutFragment;
import com.wxsoftware.developerportfolio.views.ContactFragment;
import com.wxsoftware.developerportfolio.views.SkillsFragment;
import com.wxsoftware.developerportfolio.views.WorkFragment;

/**
 * Created by alberto on 20/02/18.
 */

public class DevPagerAdapter extends FragmentPagerAdapter {

    public DevPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
       switch (position){
           case 0: return new AboutFragment();
           case 1: return new WorkFragment();
           case 2: return new SkillsFragment();
           case 3: return new ContactFragment();
           default: return null;
       }
    }

    @Override
    public int getCount() {
        return 4;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "SOBRE";
            case 1: return "EXPERIENCIA";
            case 2: return "HABILIDADES";
            case 3: return "CONTACTO";
            default: return null;
        }
    }
}
